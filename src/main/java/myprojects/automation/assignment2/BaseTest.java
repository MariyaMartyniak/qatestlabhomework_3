package myprojects.automation.assignment2;

import myprojects.automation.assignment2.utils.WebDriverLogger;
import myprojects.automation.assignment2.utils.Properties;
import myprojects.automation.assignment2.utils.WebDriverLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public abstract class BaseTest {
    private static WebDriver getDriver() {
        String browser = Properties.getBrowser();
        switch (browser) {
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(BaseTest.class.getResource("/geckodriver.exe").getFile()).getPath());
                return new FirefoxDriver();
            case "ie":
            case "internet explorer":
                System.setProperty(
                        "webdriver.ie.driver",
                        new File(BaseTest.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                return new InternetExplorerDriver();
            case "chrome":
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(BaseTest.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
        }

    }

//    public static EventFiringWebDriver getConfiguredDriver() {
//        WebDriver driver = getDriver();
//        driver.manage().window().maximize();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);
//
//        wrappedDriver.register(new EventHandler());
//
//        return wrappedDriver;
//    }

    public static EventFiringWebDriver getConfiguredDriver(){
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        EventFiringWebDriver wrappedDriver = new EventFiringWebDriver(driver);

        wrappedDriver.register(new WebDriverLogger());

        return wrappedDriver;
    }

    public static void quitDriver(WebDriver driver){
        driver.quit();
    }







/*     The method below is used to create WebDriver instance avoiding Chrome save password popup.
     Additionally the window is being maximized

    public static WebDriver getDriver() throws Exception {

        if (chromeDriver == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            Map prefs;
            prefs = new HashMap();
            prefs.put("credentials_enable_service", false);
            prefs.put("password_manager_enabled", false);
            options.setExperimentalOption("prefs", prefs);
            String driverPath = System.getProperty("user.dir") + "/driver/chromedriver.exe";
            if (driverPath == null)
                throw new Exception("Path to chrome driver is not specified");
            System.setProperty("webdriver.chrome.driver", driverPath);
            chromeDriver = new ChromeDriver(options);
        }
        return chromeDriver;
    }*/



    /*     The method below is used to create WebDriver

        public static WebDriver getDriver() throws Exception {
            String driverPath = System.getProperty("user.dir") + "/driver/chromedriver.exe";
            if (driverPath == null){
                System.setProperty("webdriver.chrome.driver", driverPath);
            throw new Exception("Path to chrome driver is not specified");
           }
            return new ChromeDriver();
        }*/





 /*   private static ChromeDriver chromeDriver;
      My first script

    public static void loginPrestashopAdmin() {
        try {
            WebDriver driver = getDriver();
            driver.get(Properties.getBaseAdminUrl());
            WebElement email = driver.findElement(By.id("email"));
            email.sendKeys("webinar.test@gmail.com");

            WebElement passwd = driver.findElement(By.name("passwd"));
            passwd.sendKeys("Xcg7299bnSmMuRLp9ITw");

            WebElement sbmtLogin = driver.findElement(By.name("submitLogin"));
            sbmtLogin.click();

            WebDriverWait wait = new WebDriverWait(driver, 10);

            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.id("quick_select")));

        } catch (Exception e) {
            e.printStackTrace();


        }
    }*/
}

