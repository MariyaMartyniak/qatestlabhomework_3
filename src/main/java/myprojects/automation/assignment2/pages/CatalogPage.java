package myprojects.automation.assignment2.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CatalogPage {


    WebDriver driver;
    private By logoutImage = By.cssSelector("span.employee_avatar_small");
    private By catalogMenu = By.xpath("//*[contains(text(), 'Каталог')]");
    private By categorySubmenu = By.xpath("//*[@id='subtab-AdminCategories']/a");
    private By addCategoryButton = By.xpath("//*[@id='page-header-desc-category-new_category']/div");
    private By nameCategoryInput = By.id("name_1");
    private String categoryName = "Twins";
    private By saveCategoryButton = By.id("category_form_submit_btn");
    private By alertCategory = By.xpath("//*[@id=\"content\"]/div[3]/div");
    private By filterByCategoryNameAsc = By.xpath("//*[@id=\"table-category\"]/thead/tr[1]/th[3]/span/a[2]/i");
    private int numberOfCategoryBefore;

    public CatalogPage(WebDriver driver) {
        this.driver = driver;
    }


    public void clickCategorySubmenu(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 35);
        wait.until(ExpectedConditions.elementToBeClickable(catalogMenu));

        WebElement catalogMenuElement = driver.findElement(catalogMenu);
        Actions action = new Actions(driver);


        action.moveToElement(catalogMenuElement).build().perform();
        wait.until(ExpectedConditions.elementToBeClickable(categorySubmenu));
        catalogMenuElement.findElement(categorySubmenu).click();
    }

    public int numberOfCategoryBefore() {
        numberOfCategoryBefore = driver.findElements(By.xpath("//table[@id='table-category']/tbody/tr[td[3]//text()[contains(., '"+categoryName+"')]]")).size();
        return numberOfCategoryBefore;
    }

    public void addCategoryButton() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable((addCategoryButton)));
        driver.findElement(addCategoryButton).click();
    }

    public void addCategory() {
        driver.findElement(nameCategoryInput).sendKeys(categoryName);
        scrollPageDown();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(saveCategoryButton));
        driver.findElement(saveCategoryButton).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(alertCategory));
        driver.findElement(alertCategory);
    }

    public void scrollPageDown() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(logoutImage));
        WebElement element = driver.findElement(saveCategoryButton);
        JavascriptExecutor executor = (JavascriptExecutor) driver;

        executor.executeScript("arguments[0].scrollIntoView(true);", element);
    }


    public void categoryTableFilteringAsc() {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(filterByCategoryNameAsc));
        driver.findElement(filterByCategoryNameAsc).click();
    }

    public void addCategoryChecking() {

        int numberOfCategoryAfter = driver.findElements(By.xpath("//table[@id='table-category']/tbody/tr[td[3]//text()[contains(., '"+categoryName+"')]]")).size();
        System.out.println(numberOfCategoryBefore);
        System.out.println(numberOfCategoryAfter);

        if (numberOfCategoryAfter == numberOfCategoryBefore + 1) {
            System.out.println("The category " + categoryName + " is added to CategoryList");
        } else
            System.out.println("The category " + categoryName + " is not added to CategoryList");
    }
}










