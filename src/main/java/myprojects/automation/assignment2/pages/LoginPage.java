package myprojects.automation.assignment2.pages;

import myprojects.automation.assignment2.BaseTest;
import myprojects.automation.assignment2.utils.Properties;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BaseTest {

    private WebDriver driver;
    //private EventFiringWebDriver driver;
    private By emailInput = By.id("email");
    private By passwordInput = By.id("passwd");
    private By loginButton = By.className("ladda-label");
    private String email = "webinar.test@gmail.com";
    private String password = "Xcg7299bnSmMuRLp9ITw";
   // private By findInput = By.id("bo_query");


    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }


    public void open() {
        driver.get(Properties.getBaseAdminUrl());
    }

    public void fillEmailInput() {
        driver.findElement(emailInput).sendKeys(email);
    }

    public void fillPasswordInput() {
        driver.findElement(passwordInput).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(loginButton).click();
    }

 /*   public void checkLogoutImage() {
        WebDriverWait wait = new WebDriverWait(driver,5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(findInput));
        driver.findElement(findInput);
    }*/





/*    My first Login



public static void main(String[] args) throws InterruptedException {
        // TODO Script to execute login and logout steps
        try {
            WebDriver driver = getDriver();

            loginPrestashopAdmin();
            driver.findElement(By.id("employee_infos")).click();
            driver.findElement(By.id("header_logout")).click();

            driver.quit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

}

