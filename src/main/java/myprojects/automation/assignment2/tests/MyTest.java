package myprojects.automation.assignment2.tests;

import myprojects.automation.assignment2.BaseTest;
import myprojects.automation.assignment2.pages.CatalogPage;
import myprojects.automation.assignment2.pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class MyTest extends BaseTest {
    public static void main(String[] args) {
        WebDriver driver = getConfiguredDriver();
        WebDriverWait wait = new WebDriverWait(driver, 5);

        LoginPage loginPage = new LoginPage(driver);


        loginPage.open();
        loginPage.fillEmailInput();
        loginPage.fillPasswordInput();
        loginPage.clickLoginButton();

        CatalogPage catalogPage = new CatalogPage(driver);
        catalogPage.clickCategorySubmenu(driver);
        catalogPage.numberOfCategoryBefore();
        catalogPage.addCategoryButton();
        catalogPage.scrollPageDown();
        catalogPage.addCategory();
        catalogPage.categoryTableFilteringAsc();
        catalogPage.addCategoryChecking();
        BaseTest.quitDriver(driver);
    }


}
